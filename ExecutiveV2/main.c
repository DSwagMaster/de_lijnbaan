//Daan van Veldhoven
//Executive code
#include <msp430g2553.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>



/* This code contains functions for:
 *-> DC-Motors (2x pwm-signal)
 *-> IR (send & receive) TODO (Daan)
 *-> LED-strip TODO
 *-> Bluetooth TODO (integration)
 *-> NFC TODO (Lisa)
 */


/*/------Pinout2------/
 * Vcc      ->    Vcc     | Gnd      ->    Gnd
 * TestLed  ->    1.0     | lf       ->    Xin(TA0.1)
 * bluet.   ->    1.1     | ...      ->    Xout
 * bluet.   ->    1.2     | Tst      ->    Tst
 * Nfc      ->    1.3     | Rst      ->    Rst
 * Nfc      ->    1.4     | Nfc      ->    1.7
 * Nfc      ->    1.5     | Nfc      ->    1.6
 * IR-send  ->    2.0     |          ->    2.5
 * rf(TA1.1)->    2.1     |          ->    2.4
 * IR-recv  ->    2.2     | BConnect ->  2.3
 */


/*------Defines------*/
//Pins
#define bt_connection           BIT3 //P2.3
#define motor_left_forward      BIT6 //P2.6 TA0.1
#define motor_right_forward     BIT1 //P2.1 TA1.1
#define IR_send                 BIT0 //P2.0
#define IR_recv                 BIT2 //P2.2
#define LEDERROR                BIT0 //p1.0
//Values
#define speed_regular           6000 //Duty-cycle (100% = 10000)
#define speed_upgrade           500 //D-cycle up by 5%
#define motor_left              TA0CCR1//Motor-left register
#define motor_right             TA1CCR1//Motor-right register
#define speed_cal               500//Steering

/*------End defines------*/

void InitUART(void);

/*------Globals------*/
int global_timer_mili = 0;
int global_timer_sec = 0;
int global_timer_min = 0;
int global_timer_hrs = 0;
float adjustmentleft = 1.0;
float adjustmentright = 1.0;
/*------End globals------*/

/*------Enumerators------*/
typedef enum{
    start,
    pause,
}general_condition;
general_condition start_pause = pause;

typedef enum{ //Used in pwm_test function, to determine the direction of the D-cycle
    up,
    down,
}pwm;
pwm up_down = up;

typedef enum{ //Used in switch_speed function, determines the overall speed.
    regular,
    lvl2,
    lvl3,
    lvl4,
    max,
}speed_bot;
speed_bot current_speed = regular;

/*-------End enumerators------*/

/*------Pragmatics------*/
#pragma vector = PORT2_VECTOR
__interrupt void inputs(void){ //Input interrupts of Port 2
//    if((P2IFG & IR_recv) != 0){
//        P1OUT |= LEDERROR;
//        __delay_cycles(500000);
//        P1OUT &= ~LEDERROR;
//    }
    P2IFG &= ~IR_recv;
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer(void){ //Timer A0 interrupts - Counting purposes and detections

    global_timer_mili += 10;      //100 ticks * 10ms = 1000ms

    if(global_timer_mili >= 1000) //1000ms -> 1sec
    {
        global_timer_mili = 0;
        global_timer_sec++;
        if(!(P2IN & bt_connection)){
            speed(0,0);
            P1OUT |= LEDERROR;
        }
        else{
            P1OUT &= ~LEDERROR;
        }
//        pwm_test();
    }
    if(global_timer_sec >= 60) //60sec -> 1min
    {
        global_timer_sec = 0;
//        global_timer_min++;
    }
//    if(global_timer_min >= 60) //60min -> 1hr
//    {
//        global_timer_min = 0;
//        global_timer_hrs++;
//    }
    TA0CTL &= ~TAIFG; //Flag low
}

#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    Bluetooth(UCA0RXBUF);
}
/*------End pragmatics------*/

/*------Main------*/
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    /*  CLOCK 1MHz  */
    if (CALBC1_1MHZ==0xFF){while(1);}
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    P1OUT = P2OUT = P2DIR = P2SEL = P2SEL2= 0; //reset registers

    TA0CTL |= TASSEL_2 | MC_1 | TAIE; //left_settings (+ISR)
    TA0CTL &= ~TAIFG;
    TA0CCTL1 |= OUTMOD_7;
    TA0CCR0 = 10000; // 1M (= 1MHz clock) / 10K = 100 ticks = 1 second
    motor_left = 0; //Duty_cycle = 0%

    TA1CTL |= TASSEL_2 | MC_1; //right_settings
    TA1CCTL1 |= OUTMOD_7;
    TA1CCR0 = 10000; // 1M (= 1MHz clock) / 10K = 100 ticks = 1 second
    motor_right = 0; //Duty_cycle = 0%

    InitUART();

    P1DIR |= LEDERROR;
    P2DIR &= ~(bt_connection | IR_recv);
    P2IE |= IR_recv;
    P2IES |= IR_recv;
    P2IFG &= ~IR_recv;
    P2DIR |= (motor_left_forward | motor_right_forward | IR_send); //output
    P2SEL |= (motor_left_forward | motor_right_forward);
    P2SEL2 &= ~(motor_left_forward | motor_right_forward);

    __enable_interrupt();

//    /*SANITY-CHECK*/
//    speed(1.0,1.0);

    while(1){
    /*TODO __low_power_mode(?)*/}

    return 0;
}
/*------End main------*/

/*-------Functions-------*/
void speed(float adjustmentleft, float adjustmentright){ //Determines the overall speed; the bot is in no_correction state
    int upgrade;
    switch(current_speed){
    case regular:
        upgrade = 0; //6000
        break;

    case lvl2:
        upgrade = 1; //6500
        break;

    case lvl3:
        upgrade = 2; //7000
        break;

    case lvl4:
        upgrade = 3; //7500
        break;

    case max:
        upgrade = 4; //8000
        break;
    }
    motor_left = (speed_regular + (speed_upgrade*upgrade))*adjustmentleft;
    motor_right = (speed_regular + (speed_upgrade*upgrade))*adjustmentright;
}
void InitUART(void){
    /*  UCA0  */
    //stap 1
    UCA0CTL1 |= UCSWRST;
    //stap 2
    //control registers
    UCA0CTL0 = 0;
    UCA0CTL1 |= (UCSSEL_3 /*| UCBRKIE*/);
    //Baudrate registers
    //N = SMCLK / 9600 = 104.17
    //UCBRx = INT(N/16) = INT(6.51) = 6
    UCA0BR0 = 6;
    UCA0BR1 = 0;
    //modulation control register
    //UCBRFx = round((N/16 - INT(N/16)) * 16) = 8
    UCA0MCTL = (UCBRF_8 | UCBRS_0 | UCOS16);
    //stap 3
    //RXD = P1.1
    P1SEL  |= (1<<1);
    P1SEL2 |= (1<<1);
    //TXD = P1.2
    P1SEL  |= (1<<2);
    P1SEL2 |= (1<<2);
    //stap 4
    UCA0CTL1 &= ~UCSWRST;

    UC0IE |= UCA0RXIE; //interrupt
}

void Bluetooth(char str)
{
    P1OUT &= ~LEDERROR;
    switch (str){
    /*Forward*/
    case 'F':
        speed(1.0,1.0);
        break;
    case 'f':
        speed(0,0);
        break;
    /*Left*/
    case 'L':
        speed(0.8,1.2);
        break;
    case 'l':
        speed(0,0);
        break;
    /*Right*/
    case 'R':
        speed(1.2,0.8);
        break;
    case 'r':
        speed(0,0);
        break;
    /*Shoot*/
    case 'S':
//        P2OUT |= IR_send;
//        __delay_cycles(500000);
//        P2OUT &= ~IR_send;
        P1OUT |=  LEDERROR;
        __delay_cycles(100000);
        P1OUT &= ~LEDERROR;
        break;
    /*Test*/
    case 'T':
        current_speed = (current_speed + 1) % 5;
        speed(1.0,1.0);
        break;

    default:
        P1OUT |= LEDERROR;
        break;
    }
}

void pwm_test(void){ //Combined with the timer interrupt, the d-cycle will automatically go up and down
    switch (up_down){
    case up:
        motor_left += 1000;
        if (motor_left >= 10000)
        {
            up_down = down;
        }
        break;
    case down:
        motor_left -= 1000;
        if (motor_left <= 1000)
        {
            up_down = up;
        }
        break;
    }
}

void wait(time)
{
    int time_copy = global_timer_sec;
    if(time == 0){
        while(global_timer_mili != 0);
    }
    else{
        time_copy = ((time_copy + time) % 60);
        while(global_timer_sec < time_copy);
    }
}
/*------End functions------*/
