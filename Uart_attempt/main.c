//Daan van Veldhoven
//Attempt at recreating the
//UART connection from ems10.
#include <msp430.h>

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	
	UCA0CTL1 |= UCSWRST; //Start initialization UART

	UCA0CTL0 = 0;
	UCA0CTL1 |= UCSSEL_2;

//	UCA0BR0 = 104; //~UCOS16
    UCA0BR0 = 6; //UCOS16
//	UCA0BR1 = ??; //yea..I have no clue

	UCA0MCTL |= UCOS16 | UCBRF_8 | UCBRS_0;
	/*
	 * BRF and BRS could be calculated through
	 * formulas on page 421 of the fam. user guide,
	 * but table 15.5 on page 425 shows the exact
	 * same results. UCOS16 needs to be enabled, 'cus
	 * we're dealing with oversampling
	 * (told in exercise and proven by formulas on 421)
	 */

    UCA0CTL1 &= ~UCSWRST; //Finish initialization UART
    char c;

    while(1){   //Echo, but doesn't seem to send yet...
        while((IFG2 & UCA0RXIFG) != 0);
        c = UCA0RXBUF;
        while((IFG2 & UCA0TXIFG) != 0);
        UCA0TXBUF = c;
    }

	return 0;
}
