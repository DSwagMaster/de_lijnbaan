//Daan van Veldhoven
//Executive code
#include <msp430g2553.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>



/* This code contains functions for:
 *-> DC-Motors (2x pwm-signal)
 *-> IR (send & receive) TODO (Daan)
 *-> LED-strip TODO
 *-> Bluetooth TODO (integration)
 *-> NFC TODO (Lisa)
 */


/*/------Pinout2------/
 * Vcc      ->    Vcc     | Gnd      ->    Gnd
 * ...      ->    1.0     | lf       ->    Xin(TA0.1)
 * bluet.   ->    1.1     | ...      ->    Xout
 * bluet.   ->    1.2     | Tst      ->    Tst
 * Nfc      ->    1.3     | Rst      ->    Rst
 * Nfc      ->    1.4     | Nfc      ->    1.7
 * Nfc      ->    1.5     | Nfc      ->    1.6
 * IR-send  ->    2.0     | Button(rem)->    2.5
 * rf(TA1.1)->    2.1     | Button(rem)->    2.4
 * IR-recv  ->    2.2     | Button(rem)->  2.3
 */


/*------Defines------*/
//Pins
#define switch_button           BIT5 //P2.5
#define start_button            BIT4 //P2.4
#define test_button             BIT3 //P2.3
#define motor_left_forward      BIT6 //P2.6 TA0.1
#define motor_right_forward     BIT1 //P2.1 TA1.1
#define IR_send                 BIT0 //P2.0
#define IR_recv                 BIT2 //P2.2
//Values
#define speed_regular           6000 //Duty-cycle (100% = 10000)
#define speed_upgrade           500 //D-cycle up by 5%
#define motor_left              TA0CCR1//Motor-left register
#define motor_right             TA1CCR1//Motor-right register
#define speed_cal               500//Steering
#define adjustmentplus          2// * speed_cal
#define adjustmentminus         1// * speed_cal

/*------End defines------*/

/*------Globals------*/
int global_timer_mili = 0;
int global_timer_sec = 0;
int global_timer_min = 0;
int global_timer_hrs = 0;

bool start_pause_state = false; //Interrupt handler
bool test_state = false;        //Interrupt handler
bool switch_speed_state = false;//Interrupt handler
/*------End globals------*/

/*------Enumerators------*/
typedef enum{
    start,
    pause,
}general_condition;
general_condition start_pause = pause;

typedef enum{ //Used in pwm_test function, to determine the direction of the D-cycle
    up,
    down,
}pwm;
pwm up_down = up;

typedef enum{ //Used in switch_speed function, determines the overall speed.
    regular,
    lvl2,
    lvl3,
    lvl4,
    max,
}speed_bot;
speed_bot current_speed = regular;

typedef enum{ //Used in line_detection and steering function, to determine the needed correction in directions to stay on track
    left_turn,
    right_turn,
    no_correction,
}direction_lr;
direction_lr direction_correction = no_correction;

typedef enum{ //Used in motor_settings function, to determine the general direction
    forward,        //Constantly
    stand_by,       //Paused, yet active
}direction_fb;
direction_fb direction = stand_by;

/*-------End enumerators------*/

/*------Pragmatics------*/
#pragma vector = PORT2_VECTOR
__interrupt void inputs(void){ //Input interrupts of Port 2

    if((P2IFG & switch_button) != 0)
    {   //Switch function
        switch_speed_state = true;
    }

    if((P2IFG & start_button) != 0)
    {   //Switches to start/stop condition
        start_pause_state = true;
    }

    if((P2IFG & test_button) != 0)
    {   //Test function
        test_state = true;
    }

    P2IFG &= ~(switch_button | start_button | test_button); //Flag low
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer(void){ //Timer A0 interrupts - Counting purposes and detections

    global_timer_mili += 10;      //100 ticks * 10ms = 1000ms

    if(global_timer_mili >= 1000) //1000ms -> 1sec
    {
        global_timer_mili = 0;
        global_timer_sec++;
//        speed();             //Check current state of the speed
//        pwm_test();
    }
    if(global_timer_sec >= 60) //60sec -> 1min
    {
        global_timer_sec = 0;
//        global_timer_min++;
    }
//    if(global_timer_min >= 60) //60min -> 1hr
//    {
//        global_timer_min = 0;
//        global_timer_hrs++;
//    }
    TA0CTL &= ~TAIFG; //Flag low
}
/*------End pragmatics------*/

/*------Main------*/
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

//1Mhz
    if (CALBC1_1MHZ==0xFF)                    // If calibration constant erased
    {
    while(1);                               // do not load, trap CPU!!
    }
    DCOCTL = 0;                               // Select lowest DCOx and MODx settings
    BCSCTL1 = CALBC1_1MHZ;                    // Set range
    DCOCTL = CALDCO_1MHZ;                     // Set DCO step + modulation */

    P2DIR = 0;
    P2SEL = 0;  //reset registers
    P2SEL2 = 0;

    TA0CTL |= TASSEL_2 | MC_1 | TAIE; //left_settings (+ISR)
    TA0CTL &= ~TAIFG;
    TA0CCTL1 |= OUTMOD_7;
    TA0CCR0 = 10000; // 1M (= 1MHz clock) / 10K = 100 ticks = 1 second
    motor_left = 0; //Duty_cycle = 0%

    TA1CTL |= TASSEL_2 | MC_1; //right_settings
    TA1CCTL1 |= OUTMOD_7;
    TA1CCR0 = 10000; // 1M (= 1MHz clock) / 10K = 100 ticks = 1 second
    motor_right = 0; //Duty_cycle = 0%

    /*BUTTONS*/
    P2DIR &= ~(switch_button | start_button | test_button); //Input at P2.5 and P2.4 and P2.3
    P2REN |= (switch_button | start_button | test_button); //Internal resistor enable
    P2OUT &= ~(switch_button | start_button | test_button); //Pull-down ^
    P2IE |= (switch_button | start_button | test_button); //Interrupt enable
    P2IES &= ~(switch_button | start_button | test_button); //Edge low -> high
    P2IFG &= ~(switch_button | start_button | test_button); //Flag low
    __enable_interrupt();

    P2DIR |= (motor_left_forward | motor_right_forward); //output
    P2SEL |= (motor_left_forward | motor_right_forward);
    P2SEL2 &= ~(motor_left_forward | motor_right_forward);

//    /*SANITY-CHECK*/
//    direction = forward;
//    motor_settings();

    while(1){
        /*PAUSE-STATE*/
        while(start_pause == pause){
            if(start_pause_state == true){
                direction_correction = no_correction;
                start_pause_condition();
                start_pause_state = false;
            }
            if(test_state == true){
                driving_test();
                test_state = false;
            }
        }

        /*START-STATE*/
        while(start_pause == start){
            if(start_pause_state == true){
                start_pause_condition();
                start_pause_state = false;
            }
            if(switch_speed_state == true){
                direction_correction = left_turn;
                steering();
                switch_speed_state = false;
            }
            if(test_state == true){
                current_speed = (current_speed + 1) % 5;
                speed();
                test_state = false;
            }
        }

    }
    return 0;
}
/*------End main------*/

/*-------Functions-------*/
void wait(time)
{
    int time_copy = global_timer_sec;
    if(time == 0){
        while(global_timer_mili != 0);
    }
    else{
        time_copy = ((time_copy + time) % 60);
        while(global_timer_sec < time_copy);
    }
}

void start_pause_condition(){ //Sets the robot into a pause- or start state
    switch(start_pause){
    case pause:             //Switches from pause -> start
        start_pause = start;
        direction = forward;
        motor_settings();
        break;
    case start:             //Switches from start -> pause
        start_pause = pause;
        direction = stand_by;
        motor_settings();
        break;
    }

}

void motor_settings(){ //Sets the right PWM-producing pins.
    switch(direction){
    case stand_by:
        motor_left = 0;
        motor_right = 0;
        break;
    case forward:
        speed();
        break;
    }
}

void steering(){ //Changes the course of the robot, with aid- and turn functions
    switch(direction_correction){

/*------Turn functions------*/
    case no_correction:
        //nothing
        break;
    case left_turn:
        motor_left -= speed_cal*adjustmentminus;
        motor_right += speed_cal*adjustmentplus;
        break;
    case right_turn:
        motor_left += speed_cal*adjustmentplus;
        motor_right -= speed_cal*adjustmentminus;
        break;
    }
}

void speed(){ //Determines the overall speed; the bot is in no_correction state
    int upgrade;
    switch(current_speed){
    case regular:
        upgrade = 0; //4000
        break;

    case lvl2:
        upgrade = 1; //5000
        break;

    case lvl3:
        upgrade = 2; //6000
        break;

    case lvl4:
        upgrade = 3; //7000
        break;

    case max:
        upgrade = 4; //8000
        break;
    }
    motor_left = speed_regular + (speed_upgrade*upgrade);
    motor_right = speed_regular + (speed_upgrade*upgrade);
}

void switch_speed(){ //If entered (through button press), the speed will change accordingly
    switch(up_down){
    case up:
        current_speed++;
        if(current_speed >= 4 | current_speed == max){
            up_down = down;
        }
        break;
    case down:
        current_speed--;
        if(current_speed <= 0 | current_speed == regular){
            up_down = up;
        }
        break;
    }
}

void driving_test(void){ //Function tests the general driving functions (Combine with button ISR)
/*------Forward------*/

    wait(0);            //Wait for round second
    direction = forward;
    motor_settings();
    wait(2);            //Keep the drive forward going for 2 seconds

    wait(0);            //Wait for round second
    direction = stand_by;
    motor_settings();
    wait(1);            //Hold still for 1 second

/*------Left/Right------*/

    wait(0);            //Wait for round second
    direction_correction = left_turn;
    steering();
    wait(2);            //Keep the left turn going for 2 seconds

    wait(0);            //Wait for round second
    direction = stand_by;
    motor_settings();
    wait(1);            //Hold still for 1 second

    wait(0);            //Wait for round second
    direction_correction = right_turn;
    steering();
    wait(2);            //Keep the right turn going for 2 seconds

    direction = stand_by;
    motor_settings();   //Halt: test successful?
}

void pwm_test(void){ //Combined with the timer interrupt, the d-cycle will automatically go up and down
    switch (up_down){
    case up:
        motor_left += 1000;
        if (motor_left >= 10000)
        {
            up_down = down;
        }
        break;
    case down:
        motor_left -= 1000;
        if (motor_left <= 1000)
        {
            up_down = up;
        }
        break;
    }
}
/*------End functions------*/
