################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Lib/Mfrc522.c \
../Lib/TI_USCI_I2C_master.c \
../Lib/i2c2.c 

C_DEPS += \
./Lib/Mfrc522.d \
./Lib/TI_USCI_I2C_master.d \
./Lib/i2c2.d 

OBJS += \
./Lib/Mfrc522.obj \
./Lib/TI_USCI_I2C_master.obj \
./Lib/i2c2.obj 

OBJS__QUOTED += \
"Lib\Mfrc522.obj" \
"Lib\TI_USCI_I2C_master.obj" \
"Lib\i2c2.obj" 

C_DEPS__QUOTED += \
"Lib\Mfrc522.d" \
"Lib\TI_USCI_I2C_master.d" \
"Lib\i2c2.d" 

C_SRCS__QUOTED += \
"../Lib/Mfrc522.c" \
"../Lib/TI_USCI_I2C_master.c" \
"../Lib/i2c2.c" 


