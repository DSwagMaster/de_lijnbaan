#include <msp430.h>

//Defines
#define LEDSEND     BIT0    //p1.0
#define LEDERROR    BIT3    //p2.3

//Declare functions
void InitUART(void);

// gevonden op https://www.embeddedrelated.com/showarticle/420.php
#pragma vector = USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    LED(UCA0RXBUF);
}

int main(void)
{
    WDTCTL = WDTPW | WDTHOLD; // stop watchdog timer

    /*  CLOCK 1MHz  */
    if (CALBC1_1MHZ==0xFF)
    {
        while(1);
    }
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    /*---LEDS---*/
    P1OUT = P2OUT = 0;
    P1DIR |= LEDSEND;
    P2DIR |= LEDERROR;

    InitUART();

    char c;

    _enable_interrupt();

    while(1)
    {
//        while((IFG2 & UCA0RXIFG) == 0);
//        LED(UCA0RXBUF);
//        while((IFG2 & UCA0TXIFG) == 0);
//        UCA0TXBUF = c;
    }
 return 0;
}

void LED(char str)
{
//    LedStatus s = (int *) str;
    P1OUT &= ~LEDSEND;
    P2OUT &= ~LEDERROR;
    switch (str){
    case 'F':
        P1OUT |=  LEDSEND;
        break;
    case 'f':
        P1OUT &= ~LEDSEND;
        break;

    case 'L':
        P1OUT |=  LEDSEND;
        break;
    case 'l':
        P1OUT &= ~LEDSEND;
        break;

    case 'R':
        P1OUT |=  LEDSEND;
        break;
    case 'r':
        P1OUT &= ~LEDSEND;
        break;

    case 'S':
        P2OUT |=  LEDERROR;
        _delay_cycles(100000);
        P2OUT &= ~LEDERROR;
        break;

    default:
        P2OUT |= LEDERROR;
        break;
    }
}

void InitUART(void){
    /*  UCA0  */
    //stap 1
    UCA0CTL1 |= UCSWRST;
    //stap 2
    //control registers
    UCA0CTL0 = 0;
    UCA0CTL1 |= (UCSSEL_3 /*| UCBRKIE*/);
    //Baudrate registers
    //N = SMCLK / 9600 = 104.17
    //UCBRx = INT(N/16) = INT(6.51) = 6
    UCA0BR0 = 6;
    UCA0BR1 = 0;
    //modulation control register
    //UCBRFx = round((N/16 - INT(N/16)) * 16) = 8
    UCA0MCTL = (UCBRF_8 | UCBRS_0 | UCOS16);
    //stap 3
    //RXD = P1.1
    P1SEL  |= (1<<1);
    P1SEL2 |= (1<<1);
    //TXD = P1.2
    P1SEL  |= (1<<2);
    P1SEL2 |= (1<<2);
    //stap 4
    UCA0CTL1 &= ~UCSWRST;

    UC0IE |= UCA0RXIE; //interrupt
}
